<?php

require __DIR__.'/sha1le.php';

$secret = sha1(uniqid(mt_rand(), 1));

$data = 'original data'.mt_rand();
$sign = sha1($secret.$data);
assert(sha1($secret.$data) === $sign);

$evil_data = 'evil data'.mt_rand();
$keylen = strlen($secret); //we neew to know secret length (or bruteforce it)
$datalen = strlen($data);

$data_chunk = pad($data, $datalen + $keylen);
$pad = substr($data_chunk, $datalen);

$evil_chunk = pad($evil_data, strlen($data_chunk) + strlen($evil_data) + $keylen);
$evil_sign = _sha1le($evil_chunk, $sign);
$hdata = $data.$pad.$evil_data;
assert(sha1($secret.$hdata) === $evil_sign);
